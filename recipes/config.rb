#
# Cookbook Name:: ircd-hybrid
# Recipe:: install
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

wd = node['ircd-hybrid']['wd']
host = node['ircd-hybrid']['config']['host']
user = node['ircd-hybrid']['config']['user']

include_recipe "ircd-hybrid::dir"

conf_file = '/etc/ircd-hybrid/ircd.conf'
new_conf_file = '/etc/ircd-hybrid/ircd.conf.new'
bk_conf_file = '/etc/ircd-hybrid/ircd.conf.bk'


old_host = 'host = "127.0.0.1";'
new_host = 'host = "' + host + '";'

old_user = 'user = "\*@127.0.0.1";'
new_user = 'user = "' + user + '";'

execute 'backup config' do
	command "cp #{conf_file} #{bk_conf_file}"
	user 'root'
	creates bk_conf_file
	action :run
end

execute 'make config' do
	command "sed -e 's/#{old_host}/#{new_host}/g' -e 's/#{old_user}/#{new_user}/g' #{conf_file} > #{new_conf_file}"
	user 'root'
	creates new_conf_file
	action :run
end

execute 'switch config' do
	command "cp #{new_conf_file} #{conf_file} && touch #{wd}/.switch_config"
	user 'root'
	creates wd + '/.switch_config'
	action :run
end
