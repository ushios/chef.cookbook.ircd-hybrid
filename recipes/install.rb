#
# Cookbook Name:: ircd-hybrid
# Recipe:: install
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

wd = node['ircd-hybrid']['wd']

include_recipe "ircd-hybrid::dir"

execute 'build-dep ircd-hybrid' do
	command 'apt-get build-dep -y ircd-hybrid'
	user 'root'
	action :run
end

package 'fakeroot' do
	action :install
end

src_dir = wd + '/src'

c_dir = "IRCDH_DIR=`ls | grep ircd-hybrid-*` && "
c_deb = "IRCDH_DEB=`ls | grep ircd-hybrid_*.deb` && "
c_lf = "LF=$(printf '\\\012_'); LF=${LF%_} && "

directory src_dir do
	action :create
	recursive true
	mode 700
	owner 'root'
	group 'root'
end

execute 'get ircd-hybrid source' do
	command "apt-get source ircd-hybrid  && touch #{wd}/.get_source"
	user 'root'
	cwd src_dir
	creates wd + '/.get_source'
	action :run
end

execute 'backup rules' do
	command c_dir + "cp ${IRCDH_DIR}/debian/rules ${IRCDH_DIR}/debian/rules.bk && touch #{wd}/.rules_bk"
	user 'root'
	cwd src_dir
	creates wd + '/.rules_bk'
	action :run
end

execute 'add OPENSSL' do
	command c_dir + c_lf + "cd ${IRCDH_DIR}/debian && sed '25s/$/'\"$LF\"'USE_OPENSSL = 1/g' rules > rules.new && touch #{wd}/.add_openssl"
	user 'root'
	cwd src_dir
	creates wd + '/.add_openssl'
	action :run
end

execute 'copy rules' do
	command c_dir + "cp ${IRCDH_DIR}/debian/rules.new ${IRCDH_DIR}/debian/rules && touch #{wd}/.rules_switch"
	user 'root'
	cwd src_dir
	creates wd + '/.rules_switch'
	action :run
end

execute 'dpkg-buildpackage' do
	command c_dir + "cd ${IRCDH_DIR} && dpkg-buildpackage -rfakeroot -uc -b && touch #{wd}/.dpkgbuildpackage"
	user 'root'
	cwd src_dir
	creates wd + '/.dpkgbuildpackage'
	action :run
end

execute 'dpkg' do
	command c_deb + "dpkg -i ${IRCDH_DEB} && touch #{wd}/.dpkg"
	user 'root'
	cwd src_dir
	creates wd + '/.dpkg'
	action :run
end



#root@precise64:‾# LF=$(printf '\\\012_')
#root@precise64:‾# LF=${LF%_}
#root@precise64:‾# sed '25s/$/'"$LF"'hogehoge/g' ircd-hybrid-7.2.2.dfsg.2/debian/rules > hogehoge.txt


# show http://www.davidxia.com/2013/03/how-to-setup-ircd-hybrid-with-ssl-on-ubuntu/
#package 'ircd-hybrid' do
#	action :install
#end