#
# Cookbook Name:: ircd-hybrid
# Recipe:: install
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

directory node['ircd-hybrid']['wd'] do
	action :create
	recursive true
	owner 'root'
	group 'root'
	mode 700
end
