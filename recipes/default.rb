#
# Cookbook Name:: ircd-hybrid
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe"ircd-hybrid::dir"
include_recipe"ircd-hybrid::install_ssls"
include_recipe"ircd-hybrid::install_dpkg"
include_recipe"ircd-hybrid::install"
include_recipe"ircd-hybrid::config"

service "ircd-hybrid" do
	action :restart
end
