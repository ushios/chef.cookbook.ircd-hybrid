default['ircd-hybrid'] = {
	'wd' => '/var/chef/ircd-hybrid',
	'config' => {
		'host' => '0.0.0.0',
		'user' => '*@*'
	}
}
